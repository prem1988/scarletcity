<?php

class profilePage extends Page
{
    
}

Class profilePage_Controller extends MemberLogin_Controller
{
    
    
        private static $allowed_actions = array (
  
        );
        
        
        public function getUserInformation() {

                //Get User Information From remote server
                $user_url = "https://api.theaetherlight.com/user";
                      
                      
                //Get Data From Remote server Using Curl
                $userJson=  MemberLogin_Controller::getAPiCallUser($user_url);
                         
                //decode JSon to Array
                $user = array();
                $user = json_decode($userJson , true);
                
                $name = $user['characterName'];
                $gender = $user['gender'];
                $episodeId = $user['episodeId'];
                $charLevelId = $user['charLevelId'];
                $createdDate = $user['createdDate'];
                $loginDate= $user['loginDate'];
                
                //Get The First Name For Charate Name
                $firstname_value = explode(" ", $name);
                $firstname =  $firstname_value[0];
                
                //Convert Date into String
                
                $datetimeCreate = explode("T",$createdDate);
                $datetimeLogin = explode("T",$loginDate);
                $createDateTimeStamp = $datetimeCreate[0];
                $LoginDateTimeStamp = $datetimeLogin[0];
                $date1=date_create($createDateTimeStamp);
                $date2=date_create($LoginDateTimeStamp);
                $diff=date_diff($date1,$date2);
                $date_diff = $diff->format("%a");

                
                $data = array(
                        'name' => $firstname,
                        'gender' => $gender,
                        'episode' =>$episodeId,
                        'level' => $charLevelId,
                        'date' => $createDateTimeStamp,
                        'diff'  =>$date_diff,
                        'logout' =>"member-login/logout"
                    
                );
                
                $output =new ArrayList(array(
                        new ArrayData($data)
                ));
                
                
                
                return $output;
        }

}
