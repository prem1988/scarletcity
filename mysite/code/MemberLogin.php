<?php
class MemberLogin extends Page
{
        private static $db = array(
                    'Username' => 'Varchar',
                    'Password' => 'Varchar'
	);
}

Class MemberLogin_Controller extends Page_Controller
{
        
        private static $allowed_actions = array (
                'MemberLoginForm',
                'handleMemberLogin',
                'loginAPiCall',
                'profilePage',
                'logout'
        );
        
        /*
         * 
         * Get Api Request Data using File Get Content PHP Method Function
         */
        private function _get_url($url)
        {
                $ret = file_get_contents($url);
                return json_decode($ret);
        }
        
         /*
         * 
         * Check cUrl information.. try to find out cUrl working status
         */
         public function _isCurl(){
                return function_exists('curl_version');
        }   
        
        /*
         * Sending Data to Remote server using api instruction 
         * used PHP default API gateway to access data
         * Posting login access details and get the server cookies
         */
        public function curlPostAPI($url, $data)
        {
                
                //converting array data into JSon readable format
                $data_json = json_encode($data);
                
                //Header Type for passing Value.. which we use as Json Format
                $headers  = array();
                $headers[] = 'Content-type: application/json; charset=utf-8;charset=UTF-8';
                $headers[] = 'Content-Length: ' . strlen($data_json);
                
                //curl is enabled
                $ch = curl_init();  
                //Set the URL to work with
                curl_setopt($ch, CURLOPT_URL, $url);
                //Sending Method using CURL PUT, POST  or Get can be used
                curl_setopt($ch, CURLOPT_POST, 1);
                //Request Sending by POST method
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                //Sending Data Method for Curl
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                //Curl Return Request information
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); 
                curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
                //Sending Header Type of JSon
                curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                //Storing Return Cookie details in txt file 
                curl_setopt($ch, CURLOPT_COOKIEJAR, '/var/www/cookie/cookie.txt');
                curl_setopt($ch, CURLOPT_COOKIEFILE, '/var/www/cookie/cookie.txt');
                // execute the request, but this time we care about the result
                $results = curl_exec($ch);
                
                
                
                if (curl_errno($ch)) {
                        // this would be your first hint that something went wrong
                        die('Couldn\'t send request: ' . curl_error($ch));
                } else {
                        // check the HTTP status code of the request
                        $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                        
                        //Check Login status is 204
                        if ($resultStatus != 204) {
                                //send Error Status
                                return $this->redirect('/');
                        }
                         elseif ($resultStatus == 403) {
                                        return $this->redirect('/');
                                }
                        else
                        {
                                 Return true;
                        }
                }
                //Curl operations finished    
                curl_close($ch);
        }
        
        /*
         * Get Data from Remote server using api instruction 
         * used PHP default API gateway to access data
         * Requesting information from api url
         */
        public function getAPiCallUser($url){
            
                        //curl is enabled
                        $ch = curl_init();  
                        //Set the URL to work with
                        curl_setopt($ch, CURLOPT_URL, $url);
                        //Curl Return Request information
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        //Get Stored Cookie details From Path
                        curl_setopt($ch, CURLOPT_COOKIEJAR, '/var/www/cookie/cookie.txt');
                        curl_setopt($ch, CURLOPT_COOKIEFILE, '/var/www/cookie/cookie.txt');
                        // execute the request, but this time we care about the result
                        $json_get = curl_exec($ch);
                        

                        //Check Is there is any error in curl
                        if (curl_errno($ch)) {
                                // this would be your first hint that something went wrong
                                die('Couldn\'t send request: ' . curl_error($ch));
                        } else {
                                // check the HTTP status code of the request
                                $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                                if ($resultStatus != 200) {
                                        //die('Request failed: HTTP status code: ' . $resultStatus);
                                        return $this->redirect('/');
                                }
                                elseif ($resultStatus == 403) {
                                        return $this->redirect('/');
                                }
                                else
                                {
                                        return $json_get;
                                }
                        }    
                        //Curl operations finished    
                        curl_close($ch);
        }   
        
        /*
         * Get Value from Web Form  using form post method
         * Send Value to remote server for login
         */
        
        public function loginAPiCall ($data, Form $form){
                
                //Checking curl status in PHP
                if ($this->_iscurl()){
                    
                        //Using the Data as Array 
                        $data = array (
                             'username' => $data['username'],
                             'password' => $data['password']
                        );
                          
                        //login Remote Access URL
                        $url = "https://api.theaetherlight.com/user/login";
                        
                        //Post Data In API request using PHP cUrl
                        $this->curlPostAPI($url,$data);
                       
                        
                        //return $this->profilePage($user);
                        return $this->redirect('member-login/profile');
                }
                         
                else{
                        echo "CURL is disabled";
                }
                         
        }
        
        public function profilePage()
        {   
                //return $this->profilePage($user);
                return $this->getRequest()->getIP();
        }
        
        public function logout()
        {   
                $fh = fopen( '/var/www/cookie/cookie.txt', 'w' );
                fclose($fh);
                 return $this->redirect('/');
        }
        
        
   
        public function MemberLoginForm()
        {              
            
                        //create form using SilverStripe method
                        $form = Form::create(
                                $this,
                                __FUNCTION__,
                                FieldList::create(
                                    TextField::create('username',''),
                                    PasswordField::create('password','')
                                ),
                                FieldList::create(
                                    FormAction::create('loginAPiCall','Login')
                                        ->setUseButtonTag(true)
                                        ->addExtraClass('btn btn-lg btn-primary btn-block')
                                ),
                                RequiredFields::create('username','password')
                        )
                        ->addExtraClass('form-style');
                        //$form->setFormAction('/member-login/handleMemberLogin');
                        //$form->setFormAction('https://api.theaetherlight.com/user/login');
                        
                        //Form Action Methos post
                        $form->setFormMethod('post');
                        
                        foreach($form->Fields() as $field) {
                            $field->addExtraClass('form-control')
                                   ->setAttribute('placeholder', $field->getName().'*');            
                        }

                        return $form;
                
        }
        
        
        public function handleMemberLogin($data, Form $form) {
               var_dump($data);
                
                if(!empty($_POST)) { 
                    var_dump($this->username);
                    
                } 
        }
        
        
        
}

