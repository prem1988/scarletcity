<?php

class CustomRequestFilter implements RequestFilter {

    public function preRequest(SS_HTTPRequest $request, Session $session, DataModel $model) {

        // if(!something) {
        //     By returning 'false' from the preRequest method, request execution will be stopped from continuing.
        //    return false;
        // }

        // we can also set any properties onto the request that we need or add any tracking
        // Foo::bar();

        // return true to continue processing.
        return true;
    }

    public function postRequest(SS_HTTPRequest $request, SS_HTTPResponse $response, DataModel $model) {
        // response is about to be sent.
        // any modifications or tracking to be done?
        // Foo::unbar();

        // return true to send the response.
        return true;
    }
}